import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/global_constants.dart';
import 'package:ucu_app/router.dart';

import 'domain/entity/ucu_user.dart';
import 'injection_container.dart' as dependencyInjection;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final FirebaseApp app = await Firebase.initializeApp(
    name: 'db2',
    options: Platform.isIOS || Platform.isMacOS
        ? FirebaseOptions(
            appId: '1:969421999291:ios:0b4557e432c37fe5d9ed9e',
            apiKey: 'AIzaSyAUUA-LaEL3fpJTf0KkfKejy6s_G0-azSE',
            projectId: 'ucuapp-db916',
            messagingSenderId: '969421999291',
            databaseURL: 'https://ucuapp-db916-default-rtdb.firebaseio.com',
          )
        : FirebaseOptions(
            appId: '1:969421999291:android:be2946d1d4ec0e86d9ed9e',
            apiKey: 'AIzaSyD9sjWy_LTQ6xILqruzsCUpk7xD3NcSIdw',
            messagingSenderId: '969421999291',
            projectId: 'ucuapp-db916',
            databaseURL: 'https://ucuapp-db916-default-rtdb.firebaseio.com',
          ),
  );

  GlobalConstants.currentUser = UCUUser.fromFirebase(FirebaseAuth.instance.currentUser);

  await dependencyInjection.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: MainRouter.generateRoute,
        initialRoute: GlobalConstants.currentUser != null
            ? MainRouter.HOME_ROUTE
            : MainRouter.LOGIN_ROUTE,
        theme: ThemeData(
          fontFamily: 'SFPro',
          brightness: Brightness.light,
          primaryColor: ColorConstants.PRIMARY_BROWN,
        ),
      ),
    );
  }
}
