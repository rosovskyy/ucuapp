import 'package:flutter/material.dart';
import 'package:ucu_app/presentation/event_detail/page/event_details_page.dart';
import 'package:ucu_app/presentation/home/page/home_page.dart';
import 'package:ucu_app/presentation/login/page/login_page.dart';
import 'package:ucu_app/presentation/register/page/register_page.dart';

class MainRouter {
  static const String LOGIN_ROUTE = '/login';
  static const String REGISTER_ROUTE = '/register';
  static const String HOME_ROUTE = '/homeRoute';
  static const String EVENT_DETAILS = '/eventDetails';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case LOGIN_ROUTE:
        return MaterialPageRoute(
          builder: (_) => LoginPage(),
        );
      case REGISTER_ROUTE:
        return MaterialPageRoute(
          builder: (_) => RegisterPage(),
        );
      case HOME_ROUTE:
        return MaterialPageRoute(
          builder: (_) => HomePage(),
        );
      case EVENT_DETAILS:
        return MaterialPageRoute(
          builder: (_) => EventDetailsPage(event: settings.arguments),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => LoginPage(),
        );
    }
  }
}
