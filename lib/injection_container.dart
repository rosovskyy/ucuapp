import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:ucu_app/data/repository/event_repository_impl.dart';
import 'package:ucu_app/domain/repository/event_repository.dart';
import 'package:ucu_app/domain/use_case/get_events_use_case.dart';
import 'package:ucu_app/domain/use_case/get_news_use_case.dart';
import 'package:ucu_app/domain/use_case/get_users_use_case.dart';
import 'package:ucu_app/domain/use_case/register_user_use_case.dart';
import 'package:ucu_app/presentation/events/bloc/bloc/events_bloc.dart';
import 'package:ucu_app/presentation/home/bloc/home_bloc.dart';
import 'package:ucu_app/presentation/info/bloc/bloc/info_bloc.dart';
import 'package:ucu_app/presentation/login/bloc/login_bloc.dart';
import 'package:ucu_app/presentation/profile/bloc/profile_bloc.dart';
import 'package:ucu_app/presentation/register/bloc/register_bloc.dart';
import 'package:ucu_app/presentation/search/bloc/bloc/search_bloc.dart';

import 'core/util/network_info.dart';
import 'data/data_source/remote_data_source.dart';
import 'data/repository/user_repository_impl.dart';
import 'domain/repository/user_repository.dart';

final serviceLocator = GetIt.instance;

Future<void> init() async {
  // Bloc
  serviceLocator.registerFactory(() => LoginBloc());
  serviceLocator.registerFactory(
      () => RegisterBloc(registerUserUseCase: serviceLocator()));

  serviceLocator.registerFactory(() => HomeBloc());
  serviceLocator.registerFactory(() => InfoBloc());
  serviceLocator.registerFactory(() => EventsBloc(
        getEventsUseCase: serviceLocator(),
        getNewsUseCase: serviceLocator(),
      ));
  serviceLocator.registerFactory(() => SearchBloc(getUsersUseCase: serviceLocator()));
  serviceLocator.registerFactory(() => ProfileBloc());

  // UseCase
  serviceLocator
      .registerLazySingleton(() => RegisterUserUseCase(serviceLocator()));
  serviceLocator
      .registerLazySingleton(() => GetEventsUseCase(serviceLocator()));
  serviceLocator.registerLazySingleton(() => GetNewsUseCase(serviceLocator()));
  serviceLocator.registerLazySingleton(() => GetUsersUseCase(serviceLocator()));

  // Repository
  serviceLocator.registerLazySingleton<UserRepository>(() => UserRepositoryImpl(
        remoteDataSource: serviceLocator(),
        networkInfo: serviceLocator(),
      ));
  serviceLocator
      .registerLazySingleton<EventRepository>(() => EventRepositoryImpl(
            remoteDataSource: serviceLocator(),
            networkInfo: serviceLocator(),
          ));

  // Data source
  serviceLocator
      .registerLazySingleton<RemoteDataSource>(() => RemoteDataSourceImpl());

  // Core
  serviceLocator.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(serviceLocator()));
  serviceLocator.registerLazySingleton(() => DataConnectionChecker());
}
