import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'package:ucu_app/core/const/global_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/core/error/exceptions.dart';
import 'package:ucu_app/data/model/event_response.dart';
import 'package:ucu_app/data/model/news_response.dart';
import 'package:ucu_app/data/model/user_response.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';

abstract class RemoteDataSource {

  Future<void> registerUser(UserResponse userRequest);
  Future<List<EventResponse>> getEvents();
  Future<List<NewsResponse>> getNews();
  Future<List<UserResponse>> getUsers();
}

class RemoteDataSourceImpl implements RemoteDataSource {
  @override
  Future<void> registerUser(UserResponse userRequest) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: userRequest.email,
        password: userRequest.password,
      );

      if (userCredential != null) {
        DatabaseReference newUserRef = FirebaseDatabase.instance.reference().child('users/${userCredential.user.uid}');

        Map userMap = {
          'fullName': userRequest.name.trim(),
          'email': userRequest.email.trim(),
          'phone': userRequest.phoneNumber.trim()
        };

        newUserRef.set(userMap);

        GlobalConstants.currentUser = UCUUser.fromFirebase(userCredential.user);
      }

      await userCredential.user.sendEmailVerification();

      return;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw ServerException(message: TextConstants.WEAK_PASSWORD);
      } else if (e.code == 'email-already-in-use') {
        throw ServerException(message: TextConstants.EMAIL_ALREADY_IN_USE);
      }
    } catch (e) {
      throw ServerException(message: TextConstants.SOMETHING_WRONG);
    }
  }

  @override
  Future<List<EventResponse>> getEvents() async {
    String stringData = await rootBundle.loadString('assets/jsons/events.json');
    final Map<String, dynamic> jsonMap = json.decode(stringData);
    final events = (jsonMap['events'] ?? []) as List;
    return events.map((json) => EventResponse.fromJson(json)).toList();
  }

  @override
  Future<List<NewsResponse>> getNews() async {
    String stringData = await rootBundle.loadString('assets/jsons/news.json');
    final Map<String, dynamic> jsonMap = json.decode(stringData);
    final events = (jsonMap['news'] ?? []) as List;
    return events.map((json) => NewsResponse.fromJson(json)).toList();
  }

  @override
  Future<List<UserResponse>> getUsers() async {
    String stringData = await rootBundle.loadString('assets/jsons/users.json');
    final Map<String, dynamic> jsonMap = json.decode(stringData);
    final users = (jsonMap['users'] ?? []) as List;
    return users.map((json) => UserResponse.fromJson(json)).toList();
  }
}