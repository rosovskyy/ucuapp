import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:ucu_app/core/error/exceptions.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/core/util/network_info.dart';
import 'package:ucu_app/data/data_source/remote_data_source.dart';
import 'package:ucu_app/data/mapper/user_mapper.dart';
import 'package:ucu_app/data/model/user_response.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';
import 'package:ucu_app/domain/repository/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final RemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  UserRepositoryImpl({
    @required this.remoteDataSource,
    @required this.networkInfo,
  });


  @override
  Future<Either<Failure, void>> createNewTask(String fullName, String email, String phoneNumber, String password) async {
    if (await networkInfo.isConnected) {
      try {
        final UserResponse userRequest = UserResponse(name: fullName, email: email, phoneNumber: phoneNumber, password: password);
        final success = await remoteDataSource.registerUser(userRequest);
        return Right(success);
      } on ServerException catch (e) {
        return Left(ServerFailure(message: e.message));
      }
    } else {
      try {
        return Left(CacheFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<UCUUser>>> getUsers() async {
    if (await networkInfo.isConnected) {
      try {
        final users = await remoteDataSource.getUsers();
        return Right(users.map((e) => UserMapper.mapUserResponse(e)).toList());
      } on ServerException catch (e) {
        return Left(ServerFailure(message: e.message));
      }
    } else {
      try {
        return Left(CacheFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

}