import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:ucu_app/core/error/exceptions.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/core/util/network_info.dart';
import 'package:ucu_app/data/data_source/remote_data_source.dart';
import 'package:ucu_app/data/mapper/event_mapper.dart';
import 'package:ucu_app/data/mapper/news_mapper.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/domain/entity/ucu_news.dart';
import 'package:ucu_app/domain/repository/event_repository.dart';

class EventRepositoryImpl implements EventRepository {
  final RemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  EventRepositoryImpl({
    @required this.remoteDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<UCUEvent>>> getEvents() async {
    if (await networkInfo.isConnected) {
      try {
        final events = await remoteDataSource.getEvents();
        return Right(events.map((e) => EventMapper.mapEventResponse(e)).toList());
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        return Left(CacheFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<UCUNews>>> getNews() async {
    if (await networkInfo.isConnected) {
      try {
        final news = await remoteDataSource.getNews();
        return Right(news.map((e) => NewsMapper.mapNewsResponse(e)).toList());
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        return Left(CacheFailure());
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
