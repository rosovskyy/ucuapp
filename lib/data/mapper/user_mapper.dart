import 'package:ucu_app/data/model/user_response.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';

class UserMapper {
  static UCUUser mapUserResponse(UserResponse userResponse) {
    return userResponse != null ? UCUUser(
       uid: userResponse.uid ?? "",
       name: userResponse.name ?? "",
       email: userResponse.email ?? "",
       phoneNumber: userResponse.phoneNumber ?? "",
       photoURL: userResponse.photoURL ?? "",
       events: [],
    ) : UCUUser.empty();
  }
}