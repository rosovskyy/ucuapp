import 'package:ucu_app/data/model/news_response.dart';
import 'package:ucu_app/domain/entity/ucu_news.dart';

class NewsMapper {
  static UCUNews mapNewsResponse(NewsResponse newsResponse) {
    return newsResponse != null ? UCUNews(
       id: newsResponse.id ?? "",
       title: newsResponse.title ?? "",
       date: DateTime.fromMillisecondsSinceEpoch(newsResponse.createdAt ?? DateTime.now().millisecondsSinceEpoch),
    ) : UCUNews.empty();
  }
}