import 'package:ucu_app/data/mapper/location_mapper.dart';
import 'package:ucu_app/data/mapper/user_mapper.dart';
import 'package:ucu_app/data/model/event_response.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';

class EventMapper {
  static UCUEvent mapEventResponse(EventResponse eventResponse) {
    return eventResponse != null ? UCUEvent(
       id: eventResponse.id ?? "",
       title: eventResponse.title ?? "",
       date: DateTime.fromMillisecondsSinceEpoch(eventResponse.date ?? DateTime.now().millisecondsSinceEpoch),
       image: eventResponse.image ?? "https://previews.123rf.com/images/bonumopus/bonumopus1603/bonumopus160300089/53156323-empty-transparent-background-with-gradient-opacity-.jpg",
       flags: eventResponse.flags ?? [],
       location: LocationMapper.mapLocationResponse(eventResponse.location),
       description: eventResponse.description ?? "",
       usersGoing: eventResponse.usersGoing.map((e) => UserMapper.mapUserResponse(e)).toList(),
    ) : UCUEvent.empty();
  }
}