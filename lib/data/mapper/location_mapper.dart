import 'package:ucu_app/data/model/location_response.dart';
import 'package:ucu_app/domain/entity/ucu_location.dart';

class LocationMapper {
  static UCULocation mapLocationResponse(LocationResponse locationResponse) {
    return locationResponse != null ? UCULocation(
       name: locationResponse.name,
       latitude: locationResponse.latitude,
       longitude: locationResponse.longitude,
    ) : UCULocation.empty();
  }
}