import 'package:equatable/equatable.dart';

class UserResponse extends Equatable {
  final String uid;
  final String name;
  final String email;
  final String phoneNumber;
  final String password;
  final String photoURL;

  UserResponse({
    this.name,
    this.email,
    this.phoneNumber,
    this.password,
    this.uid,
    this.photoURL,
  });

  factory UserResponse.fromJson(Map<String, dynamic> json) {
    return UserResponse(
      uid: json['uid'],
      name: json['name'],
      photoURL: json['photo_url'],
      email: json['email'],
      phoneNumber: json['phone_number'],
    );
  }

  @override
  List<Object> get props => [uid];
}