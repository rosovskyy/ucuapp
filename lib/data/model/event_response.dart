import 'package:ucu_app/data/model/location_response.dart';
import 'package:ucu_app/data/model/user_response.dart';

class EventResponse {

  final String id;
  final String title;
  final int date;
  final LocationResponse location;
  final String image;
  final List<String> flags;
  final String description;
  final List<UserResponse> usersGoing;

  EventResponse({
    this.id,
    this.title,
    this.date,
    this.location,
    this.image,
    this.flags,
    this.description,
    this.usersGoing,
  });

  factory EventResponse.fromJson(Map<String, dynamic> json) {
    return EventResponse(
      id: json['id'],
      title: json['title'],
      date: json['date'],
      location: LocationResponse.fromJson(json["location"]),
      image: json['image'],
      flags: List<String>.from(json['flags'] ?? []),
      description: json['description'],
      usersGoing: ((json["going"] ?? []) as List).map((e) => UserResponse.fromJson(e)).toList(),
    );
  }

  factory EventResponse.empty() {
    return EventResponse();
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'date': date,
      'location': location.toJson(),
      'image': image,
      'flags': flags,
      'description': description,
    };
  }
}