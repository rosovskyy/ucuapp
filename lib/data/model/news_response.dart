class NewsResponse {

  final String id;
  final String title;
  final int createdAt;

  NewsResponse({
    this.id,
    this.title,
    this.createdAt,
  });

  factory NewsResponse.fromJson(Map<String, dynamic> json) {
    return json != null ? NewsResponse(
      id: json['id'],
      title: json['title'],
      createdAt: json['created_at'],
    ) : NewsResponse.empty();
  }

  factory NewsResponse.empty() {
    return NewsResponse();
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'created_at': createdAt,
    };
  }
}