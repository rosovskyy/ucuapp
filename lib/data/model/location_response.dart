class LocationResponse {

  final String name;
  final double latitude;
  final double longitude;

  LocationResponse({
    this.name,
    this.latitude,
    this.longitude,
  });

  factory LocationResponse.fromJson(Map<String, dynamic> json) {
    return json != null ? LocationResponse(
      name: json['name'],
      latitude: json['latitde'],
      longitude: json['longitude'],
    ) : LocationResponse.empty();
  }

  factory LocationResponse.empty() {
    return LocationResponse();
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
    };
  }
}