import 'package:intl/intl.dart';

class DateUtil {

  static String getTimeFromDateTime(DateTime dateTime) {
    return DateFormat('hh:mm a').format(dateTime);
  }

  static String getDateFromDateTime(DateTime dateTime) {
    return DateFormat('MM/dd/yyyy').format(dateTime);
  }
}