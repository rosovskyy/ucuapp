class DimensionConstants {

  // Login
  static const double LOGIN_LOGO_IMAGE_SIZE = 200.0;
  static const double BACK_ICON_SIZE = 28.0;

  // Info
  static const double DEPARTMENT_CELL_HEIGHT = 130.0;
  static const double DEPARTMENT_CELL_WIDTH = 200.0;
}