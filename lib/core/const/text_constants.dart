class TextConstants {

  // Login
  static const String EMAIL = 'Email';
  static const String PASSWORD = 'Password';
  static const String FORGOT_PASSWORD = 'Forgot password?';
  static const String LOG_IN = 'Log In';
  static const String SIGN_IN_GOOGLE = 'Sign-In with Google';
  static const String DONT_HAVE_ACCOUNT = 'Don’t have an account yet?';
  static const String SIGN_UP = 'Sign Up';

  // Register
  static const String NEW_ACCOUNT = 'New Account';
  static const String FULL_NAME = 'Full Name';
  static const String EMAIL_ADDRESS = 'Email Address';
  static const String PHONE_NUMBER = 'Phone Number';
  static const String REGISTRATION_PROGRESS = 'Registration is in progress';

  // Forget Password
  static const String FORGOT_YOUR_PASSWORD = 'Forgot Your Password?';
  static const String ENTER_EMAIL = 'Enter your email below to receive your password instructions';
  static const String RECOVER_PASSWORD = 'Recover Password';

  // Verify Email
  static const String VERIFY_YOUR_EMAIL = 'Verify Your Email';
  static const String OPEN_MAIL = 'Hi! We have sent you the mail with the link, on which you should tap to verify the email. After that, click the button below';
  static const String VERIFY_EMAIL = 'Verify Email';
  static const String QUESTIONS = 'Questions? Contact us at';
  static const String EMAIL_NOT_VERIFIED = 'Your email is not verified';

  // Info
  static const String UCU = 'Ukrainian Catholic University';

  // Events
  static const String EVENTS = 'Events';
  static const String NEWS = 'News';
  static const String CLEAR_ALL = 'Clear All';
  static const String REGISTER = 'Register';
  static const String DISABLE = 'Disable';
  static const String EVENT_DETAILS = 'Event Details';
  static const String NO_NEWS = 'There are no news for today :(';

  // Search
  static const String SEARCH = 'Search';
  static const String USER_NAME = 'Name Surname';
  static const String EMPTY_TEXT = 'Are you looking for someone?\nTry to type his name in the placeholder';
  static const String NO_SEARCH_DATA = "Unfortunately, we couldn't find a person with that name";

  // Errors
  static const String UCU_DOMAIN_ERROR = 'Only users with ucu.edu.ua domain can sign in';
  static const String FULL_NAME_ERROR = 'Please, provide a valid full name';
  static const String EMAIL_ERROR = 'Please, provide a valid email';
  static const String PHONE_ERROR = 'Please, provide a valid phone number';
  static const String PASSWORD_ERROR = 'The password length should be greater than 7';
  static const String NO_INTERNET = 'You have no internet connection';
  static const String USER_NOT_FOUND = 'No user found for that email';
  static const String WRONG_PASSWORD = 'Wrong password provided for that user';
  static const String WEAK_PASSWORD = 'The password provided is too weak';
  static const String EMAIL_ALREADY_IN_USE = 'The account already exists for that email';
  static const String SOMETHING_WRONG = 'Something went wrong, please try again later';
}