import 'dart:ui';

class ColorConstants {

  static const Color WHITE = Color.fromRGBO(235, 235, 235, 1);

  static const Color PRIMARY_BROWN = Color.fromRGBO(129, 27, 26, 1);

  static const Color SECONDARY_TURQUOISE = Color.fromRGBO(42, 210, 213, 1);

  static const Color LIGHT_BROWN = Color.fromRGBO(168, 62, 62, 1);

  static const Color DEFAULT_GREY = Color.fromRGBO(142, 142, 142, 1);

  static const Color DEFAULT_DARK_GREY = Color.fromRGBO(118, 118, 118, 1);
}