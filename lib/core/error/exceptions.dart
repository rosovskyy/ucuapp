import 'package:flutter/cupertino.dart';

class ServerException implements Exception {
  final String message;

  ServerException({@required this.message}) : super();
}

class CacheException implements Exception {}
