import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class Failure extends Equatable {
  final String message;

  Failure({
    @required this.message,
  });
}

class ServerFailure extends Failure {
  final String message;

  ServerFailure({
    this.message = "",
  });

  @override
  List<Object> get props => [message];
}

class CacheFailure extends Failure {
  @override
  List<Object> get props => null;
}

class InvalidInputFailure extends Failure {
  @override
  List<Object> get props => null;
}
