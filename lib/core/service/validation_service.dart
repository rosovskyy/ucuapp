class ValidationService {
  static bool fullName(String text) {
    return text.length >= 3;
  }

  static bool email(String email) {
    return RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
  }

  static bool phoneNumber(String number) {
    if (number == null || number.isEmpty) {
      return false;
    }

    const pattern = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    final regExp = RegExp(pattern);

    if (!regExp.hasMatch(number)) {
      return false;
    }
    return true;
  }

  static bool password(String password) {
    return password.length >= 8;
  }

  static bool validateEmailDomain(String email) {
    final domain = email.split('@');
    return domain.length == 2 && domain[1] == "ucu.edu.ua";
  }
}