import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/domain/entity/ucu_news.dart';

class NewsList extends StatelessWidget {
  final List<UCUNews> news;

  NewsList({
    @required this.news,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        child: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: news.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                // Navigator.of(context, rootNavigator: true).pushNamed(MainRouter.EVENT_DETAILS, arguments: events[index]);
              },
              child: NewsCell(news: news[index]),
            );
          },
        ),
      ),
    );
  }
}

class NewsCell extends StatelessWidget {
  final UCUNews news;

  NewsCell({
    @required this.news,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Container(
        height: 120,
        width: double.infinity,
        decoration: BoxDecoration(
          color: ColorConstants.LIGHT_BROWN,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Text("News"),
      ),
    );
  }
}
