import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/router.dart';

class EventsList extends StatelessWidget {
  final List<UCUEvent> events;

  EventsList({
    @required this.events,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        child: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: events.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                Navigator.of(context, rootNavigator: true).pushNamed(MainRouter.EVENT_DETAILS, arguments: events[index]);
              },
              child: EventCell(event: events[index]),
            );
          },
        ),
      ),
    );
  }
}

class EventCell extends StatelessWidget {
  final UCUEvent event;

  EventCell({
    @required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Container(
        height: 120,
        width: double.infinity,
        decoration: BoxDecoration(
          color: ColorConstants.LIGHT_BROWN,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 3,
              blurRadius: 5,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Container(
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 12, bottom: 12, left: 12, right: 18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        event.title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: ColorConstants.WHITE,
                        ),
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Icon(
                            Icons.calendar_today,
                            size: 16,
                            color: ColorConstants.WHITE,
                          ),
                          SizedBox(width: 8),
                          Text(
                            'Jan 12, 2020',
                            style: TextStyle(
                              color: ColorConstants.WHITE,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Icon(
                            Icons.place,
                            size: 16,
                            color: ColorConstants.WHITE,
                          ),
                          SizedBox(width: 8),
                          Text(
                            event.location.name,
                            style: TextStyle(
                              color: ColorConstants.WHITE,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
              child: Container(
                height: double.infinity,
                width: 120,
                color: Colors.red,
                child: Image.network(
                  event.image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
