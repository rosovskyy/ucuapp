import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/presentation/events/bloc/bloc/events_bloc.dart';
import 'package:ucu_app/presentation/events/widget/events_list.dart';
import 'package:ucu_app/presentation/events/widget/events_switcher.dart';
import 'package:ucu_app/presentation/events/widget/events_welcome.dart';
import 'package:ucu_app/presentation/events/widget/news_list.dart';
import 'package:ucu_app/presentation/search/widget/search_content.dart';

class EventsContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints.tightFor(
              height: MediaQuery.of(context).size.height),
          child: Container(
            child: SafeArea(
              child: Stack(
                children: [
                  Column(
                    children: [
                      EventsWelcome(),
                      SizedBox(height: 10),
                      BlocBuilder<EventsBloc, EventsState>(
                        builder: (context, state) {
                          var switcherIndex = 0;
                          if (state is SwitcherTappedState) {
                            switcherIndex = state.index;
                          }
                          return EventsSwitcher(switcherIndex: switcherIndex);
                        },
                      ),
                      SizedBox(height: 12),
                      BlocBuilder<EventsBloc, EventsState>(
                        buildWhen: (_, currState) =>
                            currState is Loading ||
                            currState is SwitcherTappedState,
                        builder: (context, state) {
                          if (state is Loading) {
                            return Column(
                              children: [
                                SizedBox(height: 20),
                                CircularProgressIndicator(),
                              ],
                            );
                          } else if (state is SwitcherTappedState) {
                            if (state.index == 1) {
                              return NewsList(
                                  news: BlocProvider.of<EventsBloc>(context)
                                      .news);
                            }
                          }
                          return EventsList(
                              events:
                                  BlocProvider.of<EventsBloc>(context).events);
                        },
                      ),
                      SizedBox(height: 50),
                    ],
                  ),
                  BlocBuilder<EventsBloc, EventsState>(
                    buildWhen: (_, currState) =>
                        currState is SwitcherTappedState,
                    builder: (context, state) {
                      if (state is SwitcherTappedState) {
                        final bloc = BlocProvider.of<EventsBloc>(context);
                        if ((state.index == 0 && bloc.events.isEmpty) ||
                            (state.index == 1 && bloc.news.isEmpty)) {
                          return Align(
                            alignment: Alignment.center,
                            child: EmptyImage(text: TextConstants.NO_NEWS, imagePath: "empty-news-data",),
                          );
                        }
                      }
                      return Container();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
