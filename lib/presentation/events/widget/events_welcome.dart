import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/global_constants.dart';

class EventsWelcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Hello, ${GlobalConstants.currentUser.name.split(" ")[0]}!",
                    style: TextStyle(
                      fontSize: 24,
                      color: ColorConstants.PRIMARY_BROWN,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    "Let's explore what's happening in the university",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 18),
            CircleAvatar(
              radius: 35,
              backgroundImage:
                  NetworkImage(GlobalConstants.currentUser.photoURL),
              backgroundColor: Colors.transparent,
            ),
          ],
        ),
      ),
    );
  }
}
