import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/domain/entity/filter_option.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/presentation/events/bloc/bloc/events_bloc.dart';

class EventsAppBar extends StatefulWidget {
  @override
  _EventsAppBarState createState() => _EventsAppBarState();
}

class _EventsAppBarState extends State<EventsAppBar> {
  var options = <FilterOption>[];

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: Text(
        TextConstants.EVENTS,
        style: TextStyle(
          color: Colors.black,
          fontSize: 34,
          fontWeight: FontWeight.w700,
        ),
      ),
      actions: [
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {
              showFilter();
            },
            child: Image(
              alignment: Alignment.center,
              height: 34,
              width: 34,
              color: ColorConstants.PRIMARY_BROWN,
              image: AssetImage('assets/icons/filter-icon.png'),
            ),
          ),
        ),
        BlocBuilder<EventsBloc, EventsState>(
          buildWhen: (_, currState) =>
              currState is IsFavoriteState || currState is SwitcherTappedState,
          builder: (context, state) {
            return Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  BlocProvider.of<EventsBloc>(context).add(IsFavoriteEvent());
                },
                child: Visibility(
                  visible: BlocProvider.of<EventsBloc>(context).switcherIndex == 0,
                  child: Image(
                    alignment: Alignment.center,
                    height: 28,
                    width: 28,
                    color: ColorConstants.PRIMARY_BROWN,
                    image: AssetImage(
                        'assets/icons/favorite-icon${BlocProvider.of<EventsBloc>(context).isFavorite ? "-filled" : ""}.png'),
                  ),
                ),
              ),
            );
          },
        ),
      ],
      backgroundColor: Colors.white,
      centerTitle: false,
    );
  }

  void showFilter() {
    getOptions();
    final height = (options.length * 45 + 134).toDouble();

    showModalBottomSheet(
      context: context,
      elevation: 5.0,
      useRootNavigator: true,
      builder: (modalContext) {
        return StatefulBuilder(
          builder: (BuildContext _, StateSetter setState) => Container(
            width: double.infinity,
            height: height,
            color: Colors.white,
            child: Stack(
              children: [
                Positioned(
                  top: 16,
                  right: 16,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.close,
                      color: ColorConstants.PRIMARY_BROWN,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 36, top: 28, bottom: 28, right: 36),
                  child: Column(
                    children: [
                      for (int i = 0; i < options.length; i++) ...[
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              options[i].isChecked = !options[i].isChecked;
                            });
                          },
                          child: FilterOptionWidget(
                            option: options[i],
                          ),
                        ),
                        SizedBox(height: 15),
                      ],
                      SizedBox(height: 20),
                      Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  for (int i = 0; i < options.length; i++) {
                                    options[i].isChecked = false;
                                  }
                                });
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) {
                                    return ColorConstants
                                        .WHITE; // Use the component's default.
                                  },
                                ),
                              ),
                              child: Text(
                                TextConstants.CLEAR_ALL,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(width: 12),
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.pop(modalContext);
                                BlocProvider.of<EventsBloc>(context).add(
                                    ApplyFiltersEvent(
                                        filters: options
                                            .where((e) => e.isChecked)
                                            .toList()));
                              },
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) {
                                    return ColorConstants.PRIMARY_BROWN;
                                  },
                                ),
                              ),
                              child: Text(
                                TextConstants.REGISTER,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void getOptions() {
    options.clear();
    var blocOpt =
        BlocProvider.of<EventsBloc>(context).filterOptions.map((e) => e.name);
    for (UCUEvent event in BlocProvider.of<EventsBloc>(context).allEvents) {
      for (String flag in event.flags) {
        if (!options.contains(flag)) {
          var filterOpt = FilterOption(name: flag);
          if (blocOpt.contains(flag)) {
            filterOpt.isChecked = true;
          }
          options.add(filterOpt);
        }
      }
    }
  }
}

class FilterOptionWidget extends StatelessWidget {
  final FilterOption option;

  FilterOptionWidget({
    @required this.option,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Image(
          alignment: Alignment.center,
          height: 28,
          width: 28,
          image: AssetImage(
              'assets/icons/checkbox-${option.isChecked ? "checked" : "unchecked"}-icon.png'),
        ),
        SizedBox(width: 12),
        Expanded(
          child: Container(
            child: Text(
              option.name,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
