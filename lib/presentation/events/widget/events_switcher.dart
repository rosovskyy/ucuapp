import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/presentation/events/bloc/bloc/events_bloc.dart';

class EventsSwitcher extends StatelessWidget {
  final int switcherIndex;

  EventsSwitcher({
    this.switcherIndex = 0,
  });

  @override
  Widget build(BuildContext context) {
    final switcherWidth = (MediaQuery.of(context).size.width - 80) / 2;
    return Container(
      height: 50,
      width: double.infinity,
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      BlocProvider.of<EventsBloc>(context)
                          .add(SwitcherTappedEvent(index: 0));
                    },
                    child: Container(
                      child: Center(
                        child: Text(
                          TextConstants.EVENTS,
                          style: TextStyle(
                            fontSize: 18,
                            color: switcherIndex == 0
                                ? ColorConstants.PRIMARY_BROWN
                                : Colors.black,
                            fontWeight:
                                switcherIndex == 0 ? FontWeight.bold : null,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      BlocProvider.of<EventsBloc>(context)
                          .add(SwitcherTappedEvent(index: 0));
                    },
                    child: Container(
                      color: Colors.white,
                      child: Center(
                        child: GestureDetector(
                          onTap: () {
                            BlocProvider.of<EventsBloc>(context)
                                .add(SwitcherTappedEvent(index: 1));
                          },
                          child: Text(
                            TextConstants.NEWS,
                            style: TextStyle(
                              fontSize: 18,
                              color: switcherIndex == 1
                                  ? ColorConstants.PRIMARY_BROWN
                                  : Colors.black,
                              fontWeight:
                                  switcherIndex == 1 ? FontWeight.bold : null,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          AnimatedPositioned(
            duration: Duration(milliseconds: 200),
            left: switcherIndex == 0 ? 40 : 40 + switcherWidth,
            bottom: 0,
            child: Container(
              width: switcherWidth,
              height: 1,
              color: ColorConstants.PRIMARY_BROWN,
            ),
          ),
        ],
      ),
    );
  }
}
