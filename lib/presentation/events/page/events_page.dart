import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/presentation/events/bloc/bloc/events_bloc.dart';
import 'package:ucu_app/presentation/events/widget/events_appbar.dart';
import 'package:ucu_app/presentation/events/widget/events_content.dart';

import '../../../injection_container.dart';

class EventsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<EventsBloc>(
      create: (context) => serviceLocator<EventsBloc>(),
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: EventsAppBar(),
        ),
        backgroundColor: Colors.white,
        body: _buildBody(context),
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return Stack(
        children: [
          BlocBuilder<EventsBloc, EventsState>(
            buildWhen: (prevState, currState) => currState is EventsInitial,
            builder: (context, state) {
              if (state is EventsInitial) {
                BlocProvider.of<EventsBloc>(context).add(GetEventsEvent());
              }
              return EventsContent();
            },
          ),
        ],
    );
  }
}
