part of 'events_bloc.dart';

abstract class EventsEvent extends Equatable {
  const EventsEvent();

  List<Object> get props => [];
}

class GetEventsEvent extends EventsEvent {
  @override
  List<Object> get props => [];
}

class GetNewsEvent extends EventsEvent {
  @override
  List<Object> get props => [];
}

class IsFavoriteEvent extends EventsEvent {
  @override
  List<Object> get props => [];
}

class ApplyFiltersEvent extends EventsEvent {
  final List<FilterOption> filters;

  ApplyFiltersEvent({
    @required this.filters,
  });

  @override
  List<Object> get props => filters;
}

class SwitcherTappedEvent extends EventsEvent {
  final int index;

  SwitcherTappedEvent({
    @required this.index,
  });

  @override
  List<Object> get props => [index];
}
