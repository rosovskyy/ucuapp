part of 'events_bloc.dart';

abstract class EventsState extends Equatable {
  const EventsState();

  @override
  List<Object> get props => [];
}

class EventsInitial extends EventsState {}

class Loading extends EventsState {
  @override
  List<Object> get props => [];
}

class Error extends EventsState {
  @override
  List<Object> get props => [];
}

class Loaded extends EventsState {
  @override
  List<Object> get props => [];
}

class IsFavoriteState extends EventsState {
  final bool isFavorite;

  IsFavoriteState({
    @required this.isFavorite,
  });
  
  @override
  List<Object> get props => [isFavorite];
}

class SwitcherTappedState extends EventsState {
  final int index;

  SwitcherTappedState({
    @required this.index,
  });
  
  @override
  List<Object> get props => [index];
}