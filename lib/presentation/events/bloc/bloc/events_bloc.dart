import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:ucu_app/core/base/use_case.dart';
import 'package:ucu_app/core/const/global_constants.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/filter_option.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/domain/entity/ucu_news.dart';
import 'package:ucu_app/domain/use_case/get_events_use_case.dart';
import 'package:ucu_app/domain/use_case/get_news_use_case.dart';

part 'events_event.dart';
part 'events_state.dart';

class EventsBloc extends Bloc<EventsEvent, EventsState> {
  final GetEventsUseCase getEventsUseCase;
  final GetNewsUseCase getNewsUseCase;

  int dateIndex = 7;
  bool isFavorite = false;
  int switcherIndex = 0;

  EventsBloc({
    @required this.getEventsUseCase,
    @required this.getNewsUseCase,
  }) : super(EventsInitial());

  var events = <UCUEvent>[];
  var allEvents = <UCUEvent>[];
  var news = <UCUNews>[];
  var allNews = <UCUNews>[];

  var filterOptions = <FilterOption>[];

  @override
  Stream<EventsState> mapEventToState(
    EventsEvent event,
  ) async* {
    if (event is GetEventsEvent) {
      if (allEvents.isEmpty) {
        yield Loading();
        final failureOrSuccess = await getEventsUseCase(NoParams());
        yield* _transformGetEvents(failureOrSuccess);

        final newsFold = await getNewsUseCase(NoParams());
        _transformGetNews(newsFold);
      }
    } else if (event is IsFavoriteEvent) {
      isFavorite = !isFavorite;
      applyFavoriteFilteres();
      yield IsFavoriteState(isFavorite: isFavorite);
      yield SwitcherTappedState(index: switcherIndex);
    } else if (event is ApplyFiltersEvent) {
      yield Loading();
      filterOptions = event.filters;
      applyFavoriteFilteres();
      yield SwitcherTappedState(index: switcherIndex);
    } else if (event is SwitcherTappedEvent) {
      switcherIndex = event.index;
      yield SwitcherTappedState(index: switcherIndex);
    }
  }

  Stream<EventsState> _transformGetEvents(
      Either<Failure, List<UCUEvent>> failureOrSuccess) async* {
    yield failureOrSuccess.fold(
      (failure) => Error(),
      (events) {
        this.events = []..addAll(events);
        this.allEvents = []..addAll(events);
        return SwitcherTappedState(index: switcherIndex);
      },
    );
  }

  void _transformGetNews(Either<Failure, List<UCUNews>> failureOrSuccess) {
    failureOrSuccess.fold(
      (failure) => print("Error while loading news.."),
      (news) {
        this.news = [];//[]..addAll(news);
        this.allNews = [];//[]..addAll(news);
      },
    );
  }

  void applyFavoriteFilteres() {
    events.clear();
    applyIsFavorite();
    applyFilters();
  }

  void applyIsFavorite() {
    if (isFavorite) {
      for (UCUEvent event in allEvents) {
        if (GlobalConstants.currentUser.events.contains(event.id)) {
          events.add(event);
        }
      }
    } else {
      events = []..addAll(allEvents);
    }
  }

  void applyFilters() {
    final optionsString = filterOptions.map((e) => e.name).toList();
    var newEvents = <UCUEvent>[];
    if (optionsString.isNotEmpty) {
      for (UCUEvent event in events) {
        for (String flag in event.flags) {
          if (optionsString.contains(flag)) {
            newEvents.add(event);
            break;
          }
        }
      }
      events = []..addAll(newEvents);
    }
  }
}
