import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/dimension_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/core/service/validation_service.dart';
import 'package:ucu_app/presentation/common_widgets/default_button.dart';
import 'package:ucu_app/presentation/common_widgets/input_field.dart';

class RegisterContent extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  RegisterContent({
    @required this.scaffoldKey,
  });

  @override
  _RegisterContentState createState() => _RegisterContentState();
}

class _RegisterContentState extends State<RegisterContent> {
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 27, vertical: 8),
              child: Container(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 40),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Image(
                        alignment: Alignment.center,
                        height: DimensionConstants.BACK_ICON_SIZE,
                        width: DimensionConstants.BACK_ICON_SIZE,
                        image: AssetImage('assets/icons/back-icon.png'),
                      ),
                    ),
                    SizedBox(height: 50),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 23),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            TextConstants.NEW_ACCOUNT.toUpperCase(),
                            style: TextStyle(
                              fontSize: 22,
                              color: ColorConstants.DEFAULT_DARK_GREY,
                              fontFamily: 'SFPro-Light',
                            ),
                          ),
                          SizedBox(height: 50),
                          InputField(
                            labelText: TextConstants.FULL_NAME,
                            keyboardType: TextInputType.name,
                            controller: nameController,
                          ),
                          InputField(
                            labelText: TextConstants.EMAIL_ADDRESS,
                            keyboardType: TextInputType.emailAddress,
                            controller: emailController,
                          ),
                          InputField(
                            labelText: TextConstants.PHONE_NUMBER,
                            keyboardType: TextInputType.phone,
                            controller: phoneController,
                          ),
                          InputField(
                            labelText: TextConstants.PASSWORD,
                            keyboardType: TextInputType.text,
                            controller: passwordController,
                            isObscure: true,
                          ),
                          SizedBox(height: 70),
                          DefaultButton(
                            title: TextConstants.SIGN_UP,
                            onTap: () async {
                              if (await formValidation()) {
                                print("Registered");
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> formValidation() async {
    if (!await DataConnectionChecker().hasConnection) {
      showSnackBar(TextConstants.NO_INTERNET);
      return false;
    }
    if (!ValidationService.fullName(nameController.text)) {
      showSnackBar(TextConstants.FULL_NAME_ERROR);
      return false;
    }
    if (!ValidationService.email(emailController.text)) {
      showSnackBar(TextConstants.EMAIL_ERROR);
      return false;
    }
    if (!ValidationService.phoneNumber(phoneController.text)) {
      showSnackBar(TextConstants.PHONE_ERROR);
      return false;
    }
    if (!ValidationService.password(passwordController.text)) {
      showSnackBar(TextConstants.PASSWORD_ERROR);
      return false;
    }
    return true;
  }

  void showSnackBar(String title) {
    final snackBar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
