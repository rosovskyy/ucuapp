import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/presentation/register/bloc/register_bloc.dart';
import 'package:ucu_app/presentation/register/widget/register_content.dart';

import '../../../injection_container.dart';

class RegisterPage extends StatelessWidget {

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: ColorConstants.WHITE,
      body: _buildBody(context),
    );
  }

  BlocProvider<RegisterBloc> _buildBody(BuildContext context) {
    return BlocProvider<RegisterBloc>(
      create: (_) => serviceLocator<RegisterBloc>(),
      child: Stack(
        children: [
          BlocBuilder<RegisterBloc, RegisterState>(
            buildWhen: (prevState, currState) => currState is RegisterInitial,
            builder: (context, state) {
              if (state is RegisterInitial) {}
              return RegisterContent(scaffoldKey: scaffoldKey);
            },
          ),
        ],
      ),
    );
  }
}
