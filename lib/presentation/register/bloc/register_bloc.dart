import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/use_case/register_user_use_case.dart';

part 'register_event.dart';

part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final RegisterUserUseCase registerUserUseCase;

  RegisterBloc({
    @required this.registerUserUseCase,
  }) : super(RegisterInitial());

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterUserEvent) {
      yield Loading();

      final failureOrRegistered = await registerUserUseCase(
        Params(
          fullName: event.fullName,
          email: event.email,
          phoneNumber: event.phoneNumber,
          password: event.password,
        ),
      );
      yield* _eitherLoadedOrErrorStateWhileRegisterNewUser(failureOrRegistered);
    }
  }

  Stream<RegisterState> _eitherLoadedOrErrorStateWhileRegisterNewUser(Either<Failure, void> failureOrUser) async* {
    yield failureOrUser.fold(
          (failure) => ErrorState(message: failure.message),
          (success) {
        return VerifyEmail();
      },
    );
  }
}
