part of 'register_bloc.dart';

@immutable
abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterInitial extends RegisterState {
  @override
  List<Object> get props => [];
}

class VerifyEmail extends RegisterState {
  @override
  List<Object> get props => [];
}

class Loading extends RegisterState {
  @override
  List<Object> get props => [];
}

class ErrorState extends RegisterState {
  final String message;

  ErrorState({
    @required this.message,
  });

  @override
  List<Object> get props => [message];
}
