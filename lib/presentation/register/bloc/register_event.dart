part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterUserEvent extends RegisterEvent {
  final String fullName;
  final String email;
  final String phoneNumber;
  final String password;

  RegisterUserEvent({
    @required this.fullName,
    @required this.email,
    @required this.phoneNumber,
    @required this.password,
  });

  @override
  List<Object> get props => [fullName, email, phoneNumber, password];
}
