import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/presentation/home/bloc/home_bloc.dart';

class HomeTabsNavigation extends StatelessWidget {
  final int currentIndex;

  HomeTabsNavigation({
    @required this.currentIndex,
  });

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      selectedFontSize: 12.0,
      showSelectedLabels: true,
      showUnselectedLabels: true,
      // selectedItemColor: ColorConstants.PRIMARY_RED,
      // unselectedItemColor: ColorConstants.NAVIGATION_GREY,
      // backgroundColor: ColorConstants.CONTRASTING_WHITE,
      items: [
        tabBarItem('Home'),
        tabBarItem('Events'),
        tabBarItem('Search'),
        tabBarItem('Profile'),
      ],
      onTap: (index) {
        if (index == currentIndex) {
          BlocProvider.of<HomeBloc>(context).add(TabReloadEvent(index: index));
        } else {
          BlocProvider.of<HomeBloc>(context).add(TabChangedEvent(index: index));
        }
      },
    );
  }

  BottomNavigationBarItem tabBarItem(String title) {
    return BottomNavigationBarItem(
      label: title, //TextUtil.capitalize(title),
      icon: ImageIcon(
        AssetImage(getImagePath(title)),
        color: ColorConstants.DEFAULT_DARK_GREY,
      ),
      activeIcon: ImageIcon(
        AssetImage(getImagePath(title)),
        color: ColorConstants.PRIMARY_BROWN,
      ),
    );
  }

  String getImagePath(String title) {
    return 'assets/icons/${title.toLowerCase()}-icon.png';
  }
}
