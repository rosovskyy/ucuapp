part of 'home_bloc.dart';

@immutable
abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class TabChangedEvent extends HomeEvent {
  final int index;

  TabChangedEvent({
    @required this.index,
  });

  @override
  List<Object> get props => [index];
}

class TabReloadEvent extends HomeEvent {
  final int index;

  TabReloadEvent({
    @required this.index,
  });

  @override
  List<Object> get props => [index];
}
