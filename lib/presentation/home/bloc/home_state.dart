part of 'home_bloc.dart';

@immutable
abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class TabChangedState extends HomeState {
  final int index;

  TabChangedState({
    @required this.index,
  });

  @override
  List<Object> get props => [index];
}

class TabReloadState extends HomeState {
  final int id = DateTime.now().millisecondsSinceEpoch;
  final int index;

  TabReloadState({
    @required this.index,
  });

  @override
  List<Object> get props => [id, index];
}
