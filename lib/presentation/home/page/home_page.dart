import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/presentation/events/page/events_page.dart';
import 'package:ucu_app/presentation/home/bloc/home_bloc.dart';
import 'package:ucu_app/presentation/home/widget/home_tabs_navigation.dart';
import 'package:ucu_app/presentation/info/page/info_page.dart';
import 'package:ucu_app/presentation/profile/page/profile_page.dart';
import 'package:ucu_app/presentation/search/page/search_page.dart';

import '../../../injection_container.dart';

class HomePage extends StatelessWidget {
  final List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>()
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      create: (_) => serviceLocator<HomeBloc>(),
      child: Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: _buildBottomNavigationBar(context),
      ),
    );
  }

  BlocBuilder<HomeBloc, HomeState> _buildBody(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        var currentIndex = 0;
        if (state is TabChangedState) {
          currentIndex = state.index;
        }
        if (state is TabReloadState) {
          currentIndex = state.index;
          _popTab(currentIndex);
        }
        return WillPopScope(
          onWillPop: () async {
            final isFirstRouteInCurrentTab = !await _navigatorKeys[currentIndex].currentState.maybePop();
            return isFirstRouteInCurrentTab;
          },
          child: Stack(
            children: [
              _buildOffstageNavigator(context, 0, currentIndex),
              _buildOffstageNavigator(context, 1, currentIndex),
              _buildOffstageNavigator(context, 2, currentIndex),
              _buildOffstageNavigator(context, 3, currentIndex),
            ],
          ),
        );
      },
    );
  }

  BlocBuilder<HomeBloc, HomeState> _buildBottomNavigationBar(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      buildWhen: (prevState, currState) => currState is TabChangedState,
      builder: (context, state) {
        var currentIndex = 0;
        if (state is TabChangedState) {
          currentIndex = state.index;
        }
        return HomeTabsNavigation(currentIndex: currentIndex);
      },
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return [
          InfoPage(),
          EventsPage(),
          SearchPage(),
          ProfilePage(),
        ].elementAt(index);
      },
    };
  }

  Widget _buildOffstageNavigator(BuildContext context, int index, int selectedIndex) {
    var routeBuilders = _routeBuilders(context, index);
    return Offstage(
      offstage: selectedIndex != index,
      child: Navigator(
        key: _navigatorKeys[index],
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (context) => routeBuilders[routeSettings.name](context),
          );
        },
      ),
    );
  }

  void _popTab(int index) {
    bool canPop = true;
    do {
      canPop = _navigatorKeys[index].currentState.canPop();
      if (canPop) {
        _navigatorKeys[index].currentState.pop();
      }
    } while (canPop);
  }
}
