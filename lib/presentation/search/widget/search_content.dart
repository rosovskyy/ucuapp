import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/presentation/search/bloc/bloc/search_bloc.dart';
import 'package:ucu_app/presentation/search/widget/users_list.dart';

class SearchContent extends StatefulWidget {
  @override
  _SearchContentState createState() => _SearchContentState();
}

class _SearchContentState extends State<SearchContent> {
  var textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: ConstrainedBox(
          constraints: BoxConstraints.tightFor(
              height: MediaQuery.of(context).size.height),
          child: Container(
            height: double.infinity,
            child: SafeArea(
              child: Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(height: 10),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: SearchTextField(controller: textController),
                      ),
                      SizedBox(height: 10),
                      BlocBuilder<SearchBloc, SearchState>(
                        buildWhen: (_, currState) =>
                            currState is FilterByNameState,
                        builder: (context, state) {
                          if (state is FilterByNameState &&
                              state.users.isNotEmpty) {
                            return UsersList(users: state.users);
                          }
                          return Container();
                        },
                      ),
                    ],
                  ),
                  BlocBuilder<SearchBloc, SearchState>(
                    buildWhen: (_, currState) => currState is FilterByNameState,
                    builder: (context, state) {
                      if (state is FilterByNameState) {
                        if (state.users.isNotEmpty) {
                          return Container();
                        } else {
                          if (state.filter.isNotEmpty) {
                            return Align(
                            alignment: Alignment.center,
                            child: EmptyImage(text: TextConstants.NO_SEARCH_DATA, imagePath: "empty-search-data"),
                          );
                          }
                        }
                      }
                      return Align(
                        alignment: Alignment.center,
                        child: EmptyImage(text: TextConstants.EMPTY_TEXT, imagePath: "search-empty"),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class EmptyImage extends StatelessWidget {
  final String text;
  final String imagePath;

  EmptyImage({
    @required this.text,
    @required this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        margin: EdgeInsets.only(bottom: 200),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              width: 300,
              height: 250,
              fit: BoxFit.contain,
              image: AssetImage(
                  'assets/images/$imagePath.png'),
            ),
            SizedBox(height: 10),
            Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 15,
                height: 1.5,
                color: ColorConstants.DEFAULT_DARK_GREY,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SearchTextField extends StatelessWidget {
  final TextEditingController controller;

  SearchTextField({
    @required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoTextField(
      controller: controller,
      prefix: Padding(
        padding: EdgeInsets.only(left: 10),
        child: Icon(
          Icons.search,
          color: Color.fromRGBO(142, 142, 142, 1),
        ),
      ),
      placeholder: TextConstants.USER_NAME,
      style: TextStyle(
        fontSize: 18.0,
        color: Color.fromRGBO(143, 143, 147, 1),
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(239, 239, 239, 1),
        borderRadius: BorderRadius.circular(10),
      ),
      onChanged: (text) {
        BlocProvider.of<SearchBloc>(context).add(FilterByNameEvent(name: text));
      },
    );
  }
}
