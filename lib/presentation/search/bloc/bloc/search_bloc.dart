import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:ucu_app/core/base/use_case.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';
import 'package:ucu_app/domain/use_case/get_users_use_case.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final GetUsersUseCase getUsersUseCase;

  SearchBloc({
    @required this.getUsersUseCase,
  }) : super(SearchInitial());

  List<UCUUser> users = [];
  List<UCUUser> allUsers = [];

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is GetUsersEvent) {
      if (allUsers.isEmpty) {
        final failureOrSuccess = await getUsersUseCase(NoParams());
        _transformGetUsers(failureOrSuccess);
      }
    } else if (event is FilterByNameEvent) {
      if (event.name.isEmpty) { 
        users = [];
      } else {
        users = allUsers.where((user) => user.name.toLowerCase().contains(event.name.toLowerCase())).toList();
      }
      yield FilterByNameState(users: users, filter: event.name);
    }
  }

  void _transformGetUsers(Either<Failure, List<UCUUser>> failureOrSuccess) {
    failureOrSuccess.fold(
      (failure) => print("Error while loading news.."),
      (users) {
        this.allUsers = []..addAll(users);
      },
    );
  }
}
