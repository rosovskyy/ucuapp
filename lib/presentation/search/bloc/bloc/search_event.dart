part of 'search_bloc.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();

  @override
  List<Object> get props => [];
}

class GetUsersEvent extends SearchEvent {
  @override
  List<Object> get props => [];
}

class FilterByNameEvent extends SearchEvent {
  final String name;

  FilterByNameEvent({
    @required this.name,
  });

  @override
  List<Object> get props => [name];
}
