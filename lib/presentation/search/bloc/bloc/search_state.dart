part of 'search_bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class SearchInitial extends SearchState {}

class FilterByNameState extends SearchState {
  final List<UCUUser> users;
  final String filter;

  FilterByNameState({
    @required this.users,
    @required this.filter,
  });

  @override
  List<Object> get props => [users, filter];
}
