import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/presentation/search/bloc/bloc/search_bloc.dart';
import 'package:ucu_app/presentation/search/widget/search_content.dart';

import '../../../injection_container.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          title: Text(
            TextConstants.SEARCH,
            style: TextStyle(
              color: Colors.black,
              fontSize: 34,
              fontWeight: FontWeight.w700,
            ),
          ),
          centerTitle: false,
          backgroundColor: Colors.white,
        ),
        body: _buildBody(context),
      ),
    );
  }

  BlocProvider<SearchBloc> _buildBody(BuildContext context) {
    return BlocProvider<SearchBloc>(
      create: (_) => serviceLocator<SearchBloc>(),
      child: Stack(
        children: [
          BlocBuilder<SearchBloc, SearchState>(
            buildWhen: (prevState, currState) => currState is SearchInitial,
            builder: (context, state) {
              if (state is SearchInitial) {
                BlocProvider.of<SearchBloc>(context).add(GetUsersEvent());
              }
              return SearchContent();
            },
          ),
        ],
      ),
    );
  }
}
