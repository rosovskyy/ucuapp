import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/presentation/event_detail/widget/event_details_main_data.dart';

class EventDetailsContent extends StatelessWidget {
  final UCUEvent event;

  EventDetailsContent({
    @required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            width: MediaQuery.of(context).size.width,
            color: ColorConstants.WHITE,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  EventDetailsMainData(event: event),
                ],
              ),
            ),
          ),
          Positioned(
            top: 50,
            left: 24,
            child: BackButton(),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: ReserveButton(),
          ),
        ],
      ),
    );
  }
}

class ReserveButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorConstants.WHITE,
      width: double.infinity,
      padding: EdgeInsets.only(left: 20, right: 20, top: 16, bottom: 40),
      child: Container(
        decoration: BoxDecoration(
          color: ColorConstants.PRIMARY_BROWN,
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: Colors.black26, width: 2),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 14),
          child: Center(
            child: Text(
              "REGISTER",
              style: TextStyle(
                fontSize: 22,
                fontFamily: 'BarlowCondensed',
                fontWeight: FontWeight.w600,
                letterSpacing: 1,
                color: ColorConstants.WHITE,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class BackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.only(top: 8, bottom: 8, left: 4, right: 8),
        decoration: BoxDecoration(
          color: ColorConstants.PRIMARY_BROWN,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Image(
          color: ColorConstants.WHITE,
          height: 32,
          width: 32,
          image: AssetImage("assets/icons/back-icon.png"),
        ),
      ),
    );
  }
}
