import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';

class EventDetailsMainData extends StatelessWidget {
  final UCUEvent event;

  EventDetailsMainData({
    @required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: 300,
          child: Image(
            fit: BoxFit.cover,
            image: NetworkImage(event.image),
          ),
        ),
        SizedBox(height: 36),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 36),
          child: Container(
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    for (String flag in event.flags) ...[
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(252, 245, 245, 1),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text(
                          flag,
                          style: TextStyle(
                            color: ColorConstants.PRIMARY_BROWN,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                    ]
                  ],
                ),
                SizedBox(height: 12),
                Text(
                  event.title,
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 24),
                Row(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: ColorConstants.PRIMARY_BROWN,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Center(
                        child: Text(
                          "5.0",
                          style: TextStyle(
                            color: ColorConstants.WHITE,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 24),
                    Container(
                      height: 50,
                      width: 1,
                      color: Color.fromRGBO(230, 230, 230, 1),
                    ),
                    SizedBox(width: 24),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "15 JUN, 2021",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: ColorConstants.PRIMARY_BROWN,
                          ),
                        ),
                        SizedBox(height: 8),
                        Text(
                          event.location.name,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Color.fromRGBO(200, 200, 200, 1),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 30),
        Container(
          height: 1,
          width: double.infinity,
          color: Color.fromRGBO(230, 230, 230, 1),
        ),
        Container(
          width: double.infinity,
          padding: EdgeInsets.all(18),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image(
                    color: ColorConstants.PRIMARY_BROWN,
                    image: AssetImage("assets/icons/people-going-icon.png"),
                  ),
                  SizedBox(width: 8),
                  Text(
                    "${event.usersGoing.length} people going",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  for (int i = 0; i < event.usersGoing.take(3).length; i++) ...[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(18),
                      child: Container(
                        width: 36,
                        height: 36,
                        child: Image(
                          fit: BoxFit.cover,
                          image: NetworkImage(event.usersGoing[i].photoURL),
                        ),
                      ),
                    ),
                    SizedBox(width: 6),
                  ],
                  if (event.usersGoing.length > 3) ...[
                    Container(
                      width: 36,
                      height: 36,
                      decoration: BoxDecoration(
                        color: ColorConstants.SECONDARY_TURQUOISE,
                        borderRadius: BorderRadius.circular(18),
                      ),
                      child: Center(
                        child: Text(
                          "+${event.usersGoing.length - 3}",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ]
                ],
              )
            ],
          ),
        ),
        Container(
          height: 1,
          width: double.infinity,
          color: Color.fromRGBO(230, 230, 230, 1),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 36, vertical: 24),
          child: Text(
            event.description,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        SizedBox(height: 120),
      ],
    );
  }
}
