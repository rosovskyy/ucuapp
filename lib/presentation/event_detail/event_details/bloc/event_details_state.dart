part of 'event_details_bloc.dart';

abstract class EventDetailsState extends Equatable {
  const EventDetailsState();
  
  @override
  List<Object> get props => [];
}

class EventDetailsInitial extends EventDetailsState {}
