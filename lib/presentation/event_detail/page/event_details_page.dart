import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/presentation/event_detail/widget/event_details_content.dart';
import 'package:ucu_app/presentation/events/bloc/bloc/events_bloc.dart';

import '../../../injection_container.dart';

class EventDetailsPage extends StatelessWidget {
  final UCUEvent event;

  EventDetailsPage({
    @required this.event,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.WHITE,
      body: _buildBody(context),
    );
  }

  BlocProvider<EventsBloc> _buildBody(BuildContext context) {
    return BlocProvider<EventsBloc>(
      create: (_) => serviceLocator<EventsBloc>(),
      child: Stack(
        children: [
          BlocBuilder<EventsBloc, EventsState>(
            buildWhen: (prevState, currState) => currState is EventsInitial,
            builder: (context, state) {
              if (state is EventsInitial) {}
              return EventDetailsContent(event: event);
            },
          ),
        ],
      ),
    );
  }
}
