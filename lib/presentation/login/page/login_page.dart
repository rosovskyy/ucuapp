import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/presentation/login/bloc/login_bloc.dart';
import 'package:ucu_app/presentation/login/widget/login_content.dart';

import '../../../injection_container.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.WHITE,
      body: _buildBody(context),
    );
  }

  BlocProvider<LoginBloc> _buildBody(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (_) => serviceLocator<LoginBloc>(),
      child: Stack(
        children: [
          BlocBuilder<LoginBloc, LoginState>(
            buildWhen: (prevState, currState) => currState is LoginInitial,
            builder: (context, state) {
              if (state is LoginInitial) {}
              return LoginContent();
            },
          ),
        ],
      ),
    );
  }
}
