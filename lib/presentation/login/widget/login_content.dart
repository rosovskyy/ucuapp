import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/dimension_constants.dart';
import 'package:ucu_app/core/const/global_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/core/service/validation_service.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';
import 'package:ucu_app/presentation/common_widgets/default_button.dart';
import 'package:ucu_app/router.dart';

class LoginContent extends StatefulWidget {
  @override
  _LoginContentState createState() => _LoginContentState();
}

class _LoginContentState extends State<LoginContent> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Container(
        width: double.infinity,
        height: double.infinity,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 50, vertical: 8),
              child: Container(
                width: double.infinity,
                child: Column(
                  children: [
                    SizedBox(height: MediaQuery.of(context).size.height * 0.15),
                    Image(
                      alignment: Alignment.center,
                      height: DimensionConstants.LOGIN_LOGO_IMAGE_SIZE,
                      width: DimensionConstants.LOGIN_LOGO_IMAGE_SIZE,
                      image: AssetImage('assets/images/ucu-logo.png'),
                    ),
                    SizedBox(height: 69),
                    DefaultButton(
                      title: TextConstants.SIGN_IN_GOOGLE,
                      isSignIn: true,
                      onTap: () async {
                        try {
                          signInWithGoogle().then((result) {
                            if (result != null) {
                              Navigator.pushNamed(context, MainRouter.HOME_ROUTE);
                            }
                          });
                        } catch (e) {
                          print(e);
                        }
                      },
                    ),
                    SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          TextConstants.DONT_HAVE_ACCOUNT,
                          style: TextStyle(
                            color: ColorConstants.DEFAULT_GREY,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(width: 4),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(MainRouter.REGISTER_ROUTE);
                          },
                          child: Text(
                            TextConstants.SIGN_UP,
                            style: TextStyle(
                              color: ColorConstants.PRIMARY_BROWN,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<String> signInWithGoogle() async {
    await Firebase.initializeApp();

    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);

      print(user);
      if (ValidationService.validateEmailDomain(user.email)) {
        GlobalConstants.currentUser = UCUUser.fromFirebase(user);
        return '$user';
      } else {
        await googleSignIn.signOut();
        showSnackBar(TextConstants.UCU_DOMAIN_ERROR);
        return null;
      }
    }

    showSnackBar(TextConstants.USER_NOT_FOUND);
    return null;
  }

  void showSnackBar(String title) {
    final snackBar = SnackBar(
      content: Text(
        title,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 15),
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
