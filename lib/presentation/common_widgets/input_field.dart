import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';

class InputField extends StatelessWidget {

  final String labelText;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final bool isObscure;

  InputField({
    @required this.labelText,
    @required this.keyboardType,
    @required this.controller,
    this.isObscure = false,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      obscureText: isObscure,
      keyboardType: keyboardType,
      autocorrect: false,
      cursorColor: ColorConstants.PRIMARY_BROWN,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(
          // color: ColorConstants.PRIMARY_BROWN,
          fontSize: 14,
        ),
        hintStyle: TextStyle(
          color: ColorConstants.DEFAULT_GREY,
          fontSize: 10,
        ),
        focusColor: ColorConstants.PRIMARY_BROWN,
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: ColorConstants.PRIMARY_BROWN),
        ),
      ),
      style: TextStyle(
        color: ColorConstants.DEFAULT_DARK_GREY,
        fontSize: 14,
      ),
      onChanged: (text) {

      },
    );
  }
}
