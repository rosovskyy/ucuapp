import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';

class DefaultButton extends StatelessWidget {
  final String title;
  final bool isSignIn;
  final VoidCallback onTap;

  DefaultButton({
    @required this.title,
    this.isSignIn = false,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Container(
        width: double.infinity,
        height: 44,
        decoration: BoxDecoration(
          color: ColorConstants.PRIMARY_BROWN,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (isSignIn) ... [
                Image(
                  alignment: Alignment.center,
                  height: 28,
                  width: 28,
                  image: AssetImage('assets/icons/google-logo-icon.png'),
                ),
                SizedBox(width: 8),
              ],
              Text(
                title,
                style: TextStyle(
                  color: ColorConstants.WHITE,
                  fontFamily: 'SFPro-Bold',
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
