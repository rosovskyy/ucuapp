import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/text_constants.dart';
import 'package:ucu_app/presentation/info/bloc/bloc/info_bloc.dart';
import 'package:ucu_app/presentation/info/widget/info_content.dart';

import '../../../injection_container.dart';

class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: InfoPageAppBar(),
        ),
        body: _buildBody(context),
      ),
    );
  }

  BlocProvider<InfoBloc> _buildBody(BuildContext context) {
    return BlocProvider<InfoBloc>(
      create: (_) => serviceLocator<InfoBloc>(),
      child: Stack(
        children: [
          BlocBuilder<InfoBloc, InfoState>(
            buildWhen: (prevState, currState) => currState is InfoInitial,
            builder: (context, state) {
              if (state is InfoInitial) {}
              return InfoContent();
            },
          ),
        ],
      ),
    );
  }
}

class InfoPageAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      title: Row(
        children: [
          Image(
            width: 50,
            height: 50,
            image: AssetImage("assets/images/ucu-logo.png"),
          ),
          SizedBox(width: 4),
          Text(
            TextConstants.UCU,
            style: TextStyle(
              color: ColorConstants.PRIMARY_BROWN,
              fontWeight: FontWeight.w500,
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
