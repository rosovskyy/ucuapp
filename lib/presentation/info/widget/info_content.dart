import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/global_constants.dart';
import 'package:ucu_app/presentation/info/widget/additional_sections.dart';

import 'info_sections.dart';

class InfoContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: Container(
                height: double.infinity,
                width: double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 18),
                      DepSection(
                        title: "Departments",
                        departments: GlobalConstants.departments,
                      ),
                      SizedBox(height: 32),
                      DepSection(
                        title: "Student organizations",
                        departments: GlobalConstants.studentOrganizations,
                      ),
                      SizedBox(height: 32),
                      AdditionalSections(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
