import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/dimension_constants.dart';
import 'package:ucu_app/domain/entity/department.dart';

class DepSection extends StatelessWidget {
  final String title;
  final List<Department> departments;

  DepSection({
    @required this.title,
    @required this.departments,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(left: 12),
          child: Text(
            title.toUpperCase(),
            style: TextStyle(
              fontSize: 18,
              color: Colors.black87,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: 20),
        DepartmentsList(departments: departments),
      ],
    );
  }
}

class DepartmentsList extends StatelessWidget {
  final List<Department> departments;

  DepartmentsList({
    @required this.departments,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: DimensionConstants.DEPARTMENT_CELL_HEIGHT,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 4),
                scrollDirection: Axis.horizontal,
                itemCount: departments.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    child: Row(
                      children: [
                        SizedBox(width: 4),
                        DepartmentCell(department: departments[index]),
                        SizedBox(width: 4),
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DepartmentCell extends StatelessWidget {
  final Department department;

  DepartmentCell({
    @required this.department,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: DimensionConstants.DEPARTMENT_CELL_HEIGHT,
      width: DimensionConstants.DEPARTMENT_CELL_WIDTH,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: <Color>[
            // ColorConstants.PRIMARY_BROWN,
            // ColorConstants.LIGHT_BROWN,
            Color(0xFFcb2d3e),
            Color(0xFFef473a),
          ],
        ),
      ),
      child: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              department.title,
              style: TextStyle(
                fontSize: 18,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 6),
            Expanded(
              child: Container(
                child: Text(
                  department.description,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
