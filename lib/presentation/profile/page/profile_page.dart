import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/presentation/profile/bloc/profile_bloc.dart';
import 'package:ucu_app/presentation/profile/widget/profile_content.dart';

import '../../../injection_container.dart';

class ProfilePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.WHITE,
      body: _buildBody(context),
    );
  }

  BlocProvider<ProfileBloc> _buildBody(BuildContext context) {
    return BlocProvider<ProfileBloc>(
      create: (_) => serviceLocator<ProfileBloc>(),
      child: Stack(
        children: [
          BlocBuilder<ProfileBloc, ProfileState>(
            buildWhen: (prevState, currState) => currState is ProfileInitial,
            builder: (context, state) {
              if (state is ProfileInitial) {}
              return ProfileContent();
            },
          ),
        ],
      ),
    );
  }
}
