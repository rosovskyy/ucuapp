import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/presentation/profile/widget/profile_card.dart';

import '../../../router.dart';

class ProfileContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          physics: NeverScrollableScrollPhysics(),
          child: ConstrainedBox(
            constraints: BoxConstraints.tightFor(height: MediaQuery.of(context).size.height),
            child: Container(
              height: double.infinity,
              child: Stack(
                children: [
                  Column(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height * 0.3,
                        width: double.infinity,
                        color: ColorConstants.PRIMARY_BROWN,
                      ),
                      SizedBox(height: 300),
                      GestureDetector(
                        onTap: () async {
                          final GoogleSignIn googleSignIn = GoogleSignIn();
                          await googleSignIn.signOut();
                          await FirebaseAuth.instance.signOut();
                          Navigator.of(context, rootNavigator: true).pushReplacementNamed(MainRouter.LOGIN_ROUTE);
                        },
                        child: Text(
                          'Log Out',
                        ),
                      ),
                    ],
                  ),

                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.15,
                    left: 40,
                    right: 40,
                    child: ProfileCard(),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
