import 'package:flutter/material.dart';
import 'package:ucu_app/core/const/color_constants.dart';
import 'package:ucu_app/core/const/global_constants.dart';

class ProfileCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'STUDENT',
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w100,
            letterSpacing: 2,
            color: Color.fromRGBO(231, 216, 216, 1),
          ),
        ),
        SizedBox(height: 18),
        Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: BoxDecoration(
            color: ColorConstants.WHITE,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.25),
                offset: Offset(0, 4),
                blurRadius: 6,
                spreadRadius: 1,
              )
            ],
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SizedBox(height: 22),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.edit,
                          size: 20,
                          color: Colors.transparent,
                        ),
                        CircleAvatar(
                          radius: 60,
                          backgroundImage: NetworkImage(GlobalConstants.currentUser.photoURL),
                          backgroundColor: Colors.transparent,
                        ),
                        Icon(
                          Icons.edit,
                          size: 20,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 18),
                  Text(
                    GlobalConstants.currentUser.name,
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.37,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
