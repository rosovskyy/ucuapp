import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:ucu_app/core/base/use_case.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/repository/user_repository.dart';

class RegisterUserUseCase implements UseCase<void, Params> {
  final UserRepository repository;

  RegisterUserUseCase(this.repository);

  @override
  Future<Either<Failure, void>> call(Params params) async {
    return await repository.createNewTask(
        params.fullName, params.email, params.phoneNumber, params.password);
  }
}


class Params extends Equatable {
  final String fullName;
  final String email;
  final String phoneNumber;
  final String password;

  Params({
    @required this.fullName,
    @required this.email,
    @required this.phoneNumber,
    @required this.password,
  });

  @override
  List<Object> get props => [fullName, email, phoneNumber, password];
}
