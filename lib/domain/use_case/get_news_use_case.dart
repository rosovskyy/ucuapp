import 'package:dartz/dartz.dart';
import 'package:ucu_app/core/base/use_case.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/ucu_news.dart';
import 'package:ucu_app/domain/repository/event_repository.dart';

class GetNewsUseCase implements UseCase<List<UCUNews>, NoParams> {

  final EventRepository repository;

  GetNewsUseCase(this.repository);

  @override
  Future<Either<Failure, List<UCUNews>>> call(NoParams params) async {
    return await repository.getNews();
  }
}