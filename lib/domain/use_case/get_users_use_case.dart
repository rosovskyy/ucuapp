import 'package:dartz/dartz.dart';
import 'package:ucu_app/core/base/use_case.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';
import 'package:ucu_app/domain/repository/user_repository.dart';

class GetUsersUseCase implements UseCase<List<UCUUser>, NoParams> {

  final UserRepository repository;

  GetUsersUseCase(this.repository);

  @override
  Future<Either<Failure, List<UCUUser>>> call(NoParams params) async {
    return await repository.getUsers();
  }
}