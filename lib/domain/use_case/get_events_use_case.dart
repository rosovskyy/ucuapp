import 'package:dartz/dartz.dart';
import 'package:ucu_app/core/base/use_case.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/domain/repository/event_repository.dart';

class GetEventsUseCase implements UseCase<List<UCUEvent>, NoParams> {

  final EventRepository repository;

  GetEventsUseCase(this.repository);

  @override
  Future<Either<Failure, List<UCUEvent>>> call(NoParams params) async {
    return await repository.getEvents();
  }
}