import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class UCULocation extends Equatable {
  final String name;
  final double latitude;
  final double longitude;

  UCULocation({
    @required this.name,
    @required this.latitude,
    @required this.longitude,
  });

  factory UCULocation.empty() {
    return UCULocation(
      name: '',
      latitude: 0.0,
      longitude: 0.0,
    );
  }

  @override
  List<Object> get props => [name];
}
