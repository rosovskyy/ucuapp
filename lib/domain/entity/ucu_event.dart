import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:ucu_app/domain/entity/ucu_location.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';

class UCUEvent extends Equatable {
  final String id;
  final String title;
  final DateTime date;
  final UCULocation location;
  final String image;
  final List<String> flags;
  final String description;
  final List<UCUUser> usersGoing;

  UCUEvent({
    @required this.id,
    @required this.title,
    @required this.date,
    @required this.location,
    @required this.image,
    @required this.flags,
    @required this.description,
    @required this.usersGoing,
  });

  factory UCUEvent.empty() {
    return UCUEvent(
      id: '',
      title: '',
      date: DateTime.now(),
      location: UCULocation.empty(),
      image: 'https://previews.123rf.com/images/bonumopus/bonumopus1603/bonumopus160300089/53156323-empty-transparent-background-with-gradient-opacity-.jpg',
      flags: [],
      description: "",
      usersGoing: [],
    );
  }

  @override
  List<Object> get props => [id];
}
