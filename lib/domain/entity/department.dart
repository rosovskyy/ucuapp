import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class Department extends Equatable {
  final String title;
  final String description;

  Department({
    @required this.title,
    @required this.description,
  });

  factory Department.empty() {
    return Department(
      title: '',
      description: '',
    );
  }

  @override
  List<Object> get props => [title];
}