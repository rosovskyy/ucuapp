import 'package:flutter/foundation.dart';

class FilterOption {
  final String name;
  bool isChecked;

  FilterOption({
    @required this.name,
    this.isChecked = false,
  });
}
