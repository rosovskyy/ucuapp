import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class UCUNews extends Equatable {
  final String id;
  final String title;
  final DateTime date;

  UCUNews({
    @required this.id,
    @required this.title,
    @required this.date,
  });

  factory UCUNews.empty() {
    return UCUNews(
      id: '',
      title: '',
      date: DateTime.now(),
    );
  }

  @override
  List<Object> get props => [id];
}
