import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

class UCUUser {
  final String uid;
  final String name;
  final String email;
  final String phoneNumber;
  final String photoURL;
  List<String> events;

  UCUUser({
    @required this.uid,
    @required this.name,
    @required this.email,
    @required this.phoneNumber,
    @required this.photoURL,
    @required this.events,
  });

  factory UCUUser.fromFirebase(User user) {
    return UCUUser(
      uid: user.uid,
      name: user.displayName,
      email: user.email,
      phoneNumber: user.phoneNumber,
      photoURL: user.photoURL,
      events: ["1", "4"],
    );
  }

  factory UCUUser.empty() {
    return UCUUser(
      uid: "",
      name: "",
      email: "",
      phoneNumber: "",
      photoURL: "",
      events: []
    );
  }

  factory UCUUser.random() {
    return UCUUser(
      uid: "5123123",
      name: "Petro Franchuk",
      email: "franchuk@gmail.com",
      phoneNumber: "012312312",
      photoURL: "https://avatars.githubusercontent.com/u/35275869?v=4",
      events: []
    );
  }
}
