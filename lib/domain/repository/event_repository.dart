import 'package:dartz/dartz.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/ucu_event.dart';
import 'package:ucu_app/domain/entity/ucu_news.dart';

abstract class EventRepository {
  Future<Either<Failure, List<UCUEvent>>> getEvents();
  Future<Either<Failure, List<UCUNews>>> getNews();
}