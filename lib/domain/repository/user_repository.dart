import 'package:dartz/dartz.dart';
import 'package:ucu_app/core/error/failures.dart';
import 'package:ucu_app/domain/entity/ucu_user.dart';

abstract class UserRepository {
  Future<Either<Failure, void>> createNewTask(
      String fullName, String email, String phoneNumber, String password);

  Future<Either<Failure, List<UCUUser>>> getUsers();
}
